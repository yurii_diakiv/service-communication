﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace BSLINQ
{
    //class Project
    //{
    //    //[JsonProperty("id")]
    //    public int Id { get; set; }
    //    //[JsonProperty("name")]
    //    public string Name { get; set; }
    //    //[JsonProperty("description")]
    //    public string Description { get; set; }
    //    //[JsonProperty("created_at")]
    //    public DateTime CreatedAt { get; set; }
    //   // [JsonProperty("deadline")]
    //    public DateTime Deadline { get; set; }
    //   // [JsonProperty("author_id")]
    //    public int? AuthorId { get; set; }
    //    //[JsonProperty("team_id")]
    //    public int? TeamId { get; set; }

    //    public override string ToString()
    //    {
    //        return $"id : {Id}\nname : {Name}\ndescription : {Description}\ncreated_at : {CreatedAt}\ndeadline : {Deadline}\n" +
    //            $"author_id : {AuthorId}\nteam_id : {TeamId}\n";
    //    }
    //}
    class Project
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public DateTime created_at { get; set; }
        public DateTime deadline { get; set; }
        public int? author_id { get; set; }
        public int? team_id { get; set; }

        public override string ToString()
        {
            return $"id : {id}\nname : {name}\ndescription : {description}\ncreated_at : {created_at}\ndeadline : {deadline}\n" +
                $"author_id : {author_id}\nteam_id : {team_id}\n";
        }
    }
}
