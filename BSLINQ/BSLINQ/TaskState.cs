﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BSLINQ
{
    class TaskState
    {
        public int id { get; set; }
        public string value { get; set; }

        public override string ToString()
        {
            return $"id : {id}\nvalue : {value}\n";
        }
    }
}
