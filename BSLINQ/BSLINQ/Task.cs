﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BSLINQ
{
    public enum State { Created, Started, Finished, Canceled }
    class Task
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public DateTime created_at { get; set; }
        public DateTime finished_at { get; set; }
        public int state { get; set; }

        //public State state { get; set; }???
        public int? project_id { get; set; }
        public int? performer_id { get; set; }

        public override string ToString()
        {
            return $"id :{id}\nname : {name}\ndescription : {description}\ncreated_at : {created_at}\nfinished_at : {finished_at}\n" +
                $"state : {state}\nproject_id : {project_id}\nperformer_id : {performer_id}\n";
        }
    }
}
