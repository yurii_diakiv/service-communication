﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BSLINQ
{
    class User
    {
        public int id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public DateTime birthday { get; set; }
        public DateTime registred_at { get; set; }
        public int? team_id { get; set; }

        public override string ToString()
        {
            return $"id : {id}\nfirst_name : {first_name}\nlast_name : {last_name}\nemail : {email}\nbirthday : {birthday}\n" +
                $"registred_at : {registred_at}\nteam_id : {team_id}\n";
        }
    }
}
