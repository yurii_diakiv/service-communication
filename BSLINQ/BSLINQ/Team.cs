﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BSLINQ
{
    class Team
    {
        public int id { get; set; }
        public string name { get; set; }
        public DateTime created_at { get; set; }

        public override string ToString()
        {
            return $"id : {id}\nname : {name}\ncreated_at : {created_at}\n";
        }
    }
}
