﻿using System;
using System.Collections.Generic;
using System.Net;
using Newtonsoft.Json;
using System.Linq;
using Microsoft.AspNetCore.SignalR.Client;


namespace BSLINQ
{

    class Program
    {
        static public void GetNotification(string value)
        {
            Console.WriteLine(value);
        }
        static async void StartConnection()
        {
            HubConnection connection = new HubConnectionBuilder()
                   .WithUrl("http://localhost:58511/linqhub")
                   .Build();
            await connection.StartAsync();
            connection.On<string>("GetNotification", value => Console.WriteLine(value));
        }

        static void GetData<T>(string url, ref List<T> coll)
        {
            using (var webClient = new WebClient())
            {
                var responce = webClient.DownloadString(url);
                coll =  JsonConvert.DeserializeObject<List<T>>(responce);
            }
        }
        static void Main(string[] args)
        {
            StartConnection();
            Console.ReadKey();



            List<Project> projects = new List<Project>();
            List<Task> tasks = new List<Task>();
            List<TaskState> taskStates = new List<TaskState>();
            List<Team> teams = new List<Team>();
            List<User> users = new List<User>();

            //GetData("http://localhost:58511/api/projects", ref projects);
            //GetData("http://localhost:58511/api/tasks", ref tasks);
            //GetData("http://localhost:58511/api/taskstates", ref taskStates);
            //GetData("http://localhost:58511/api/teams", ref teams);
            //GetData("http://localhost:58511/api/users", ref users);

            //GetData("https://bsa2019.azurewebsites.net/api/Projects", ref projects);
            //GetData("https://bsa2019.azurewebsites.net/api/Tasks", ref tasks);
            //GetData("https://bsa2019.azurewebsites.net/api/TaskStates", ref taskStates);
            //GetData("https://bsa2019.azurewebsites.net/api/Teams", ref teams);
            //GetData("https://bsa2019.azurewebsites.net/api/Users", ref users);

            //foreach (var i in users)
            //{
            //    Console.WriteLine(i);
            //}

            //FirstTask(42);
            //SecondTask(20);
            //ThirdTask(15);
            //FourthTask();
            //FifthTask();
            //SixthTask(20);
            //SeventhTask(31);

            void FirstTask(int id)
            {
                Console.WriteLine("---First task---");
                Console.WriteLine();

                var first = from project in projects
                            join task in tasks on project.id equals task.project_id
                            where task.performer_id == id
                            group task by project into gTbP
                            select new { gTbP.Key, countOfTasks = gTbP.Count() };

                foreach (var i in first)
                {
                    Console.WriteLine("Project:");
                    Console.WriteLine(i.Key);
                    Console.Write("Count of tasks: ");
                    Console.WriteLine(i.countOfTasks);
                    Console.WriteLine();
                }
            }

            void SecondTask(int id)
            {
                Console.WriteLine("---Second task---");
                Console.WriteLine();

                var second = tasks.Where(t => t.performer_id == id && t.name.Length < 45);

                foreach (var i in second)
                {
                    Console.WriteLine(i);
                }
            }

            void ThirdTask(int id)
            {
                Console.WriteLine("---Third task---");
                Console.WriteLine();

                var third = from task in tasks
                            where task.state == 2 && task.finished_at.Year == 2019 && task.performer_id == id
                            select new { task.id, task.name };

                foreach (var i in third)
                {
                    Console.Write("id: ");
                    Console.WriteLine(i.id);
                    Console.WriteLine("name: ");
                    Console.WriteLine(i.name);
                    Console.WriteLine();
                }
            }

            void FourthTask()
            {
                Console.WriteLine("---Fourth task---");
                Console.WriteLine();

                var fourth = from team in teams
                             join user in users on team.id equals user.team_id
                             group user by team into gUbT
                             select new
                             {
                                 gUbT.Key.id,
                                 gUbT.Key.name,
                                 users = gUbT.Where(u => DateTime.Now.Year - u.birthday.Year > 12).OrderByDescending(u => u.registred_at)
                             };

                foreach (var i in fourth)
                {
                    Console.Write("Team id: ");
                    Console.WriteLine(i.id);
                    Console.Write("Name: ");
                    Console.WriteLine(i.name);
                    Console.WriteLine("List of users: ");
                    foreach (var j in i.users)
                    {
                        Console.WriteLine(j);
                    }
                }
            }

            void FifthTask()
            {
                Console.WriteLine("---Fifth task---");
                Console.WriteLine();

                var fifth = from user in users
                            join task in tasks on user.id equals task.performer_id
                            group task by user into gTbU
                            orderby gTbU.Key.first_name
                            select new { gTbU.Key.first_name, tasks = gTbU.OrderByDescending(t => t.name.Length) };
                foreach (var i in fifth)
                {
                    Console.Write("user name: ");
                    Console.WriteLine(i.first_name);
                    Console.WriteLine("tasks: ");
                    foreach (var j in i.tasks)
                    {
                        Console.WriteLine(j);
                    }
                }
            }

            void SixthTask(int id)
            {
                Console.WriteLine("---Sixth task---");
                Console.WriteLine();

                var sixth = from project in projects
                            join task in tasks on project.id equals task.project_id
                            join user in users on task.performer_id equals user.id
                            where user.id == id
                            group project by user into gPbU
                            select new
                            {
                                user = gPbU.Key,
                                lastProject = gPbU.OrderBy(p => p.created_at).Last(),
                                lPcountTasks = tasks.Where(t => t.project_id == gPbU.OrderBy(p => p.created_at).Last().id).Count(),
                                notFinishedTasks = tasks.Where(t => t.performer_id == gPbU.Key.id && t.state != 2).Count(),
                                theLongestTask = tasks.OrderBy(t => t.finished_at - t.created_at).Where(t => t.performer_id == id).Last()
                            };

                foreach (var i in sixth)
                {
                    Console.WriteLine("User: ");
                    Console.WriteLine(i.user);
                    Console.WriteLine("Last project: ");
                    Console.WriteLine(i.lastProject);
                    Console.Write("Count of tasks in the last project: ");
                    Console.WriteLine(i.lPcountTasks);
                    Console.WriteLine();
                    Console.Write("Count cenceled or not finished tasks: ");
                    Console.WriteLine(i.notFinishedTasks);
                    Console.WriteLine("The longest task: ");
                    Console.WriteLine(i.theLongestTask);
                }
            }

            void SeventhTask(int id)
            {
                Console.WriteLine("---Seventh task---");
                Console.WriteLine();

                var seventh = from project in projects
                              join task in tasks on project.id equals task.project_id
                              where project.id == id
                              group task by project into gTbP
                              select new
                              {
                                  project = gTbP.Key,
                                  theLngTaskByDesc = gTbP.OrderBy(t => t.description.Length).Last(),
                                  theShrtTaskByName = gTbP.OrderBy(t => t.name.Length).First(),
                                  countOfUsersInTeam = users.Where(u => u.team_id == gTbP.Key.team_id &&
                                        (gTbP.Key.description.Length > 25 || tasks.Where(t => t.project_id == gTbP.Key.id).Count() < 3)).Count()
                              };
                foreach (var i in seventh)
                {
                    Console.WriteLine("Project: ");
                    Console.WriteLine(i.project);
                    Console.WriteLine("The longest task by description: ");
                    Console.WriteLine(i.theLngTaskByDesc);
                    Console.WriteLine("The shortest task by name: ");
                    Console.WriteLine(i.theShrtTaskByName);
                    Console.Write("Count of users in team: ");
                    Console.WriteLine(i.countOfUsersInTeam);
                }
            }
        }
    }
}
