﻿using System;
using System.Collections.Generic;
using System.Text;
using RabbitMQ.Client;
using QueueServices.Interfaces;
using QueueServices.Models;

namespace QueueServices.Services
{
    public class MessageProducerScopeFactory
    {
        private readonly /*IConnectionFactory*/ConnectionFactory connectionFactory;

        public MessageProducerScopeFactory(/*IConnectionFactory*/ConnectionFactory connFactory)
        {
            connectionFactory = connFactory;
        }

        public /*IMessageProducerScope*/MessageProducerScope Open(MessageScopeSettings messageScopeSettings)
        {
            return new MessageProducerScope(connectionFactory, messageScopeSettings);
        }
    }
}
