﻿using System;
using System.Collections.Generic;
using System.Text;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using QueueServices.Interfaces;
using QueueServices.Models;
using QueueServices.Services;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace Worker
{
    public class MessageService : IDisposable
    {
        private readonly /*IMessageConsumerScope*/MessageConsumerScope messageConsumerScope;
        private readonly /*IMessageProducerScope*/MessageProducerScope messageProducerScope;

        public MessageService(
            /*IMessageConsumerScopeFactory*/MessageConsumerScopeFactory messageConsumerScopeFactory,
            /*IMessageProducerScopeFactory*/MessageProducerScopeFactory messageProducerScopeFactory)
        {
            messageConsumerScope = messageConsumerScopeFactory.Connect(new MessageScopeSettings
            {
                ExchangeName = "ServerExchange",
                ExchangeType = ExchangeType.Topic,
                QueueName = "SendValueQueue",
                RoutingKey = "*.queue.#"
            });

            messageConsumerScope.MessageConsumer.Received += MessageReceived;

            messageProducerScope = messageProducerScopeFactory.Open(new MessageScopeSettings
            {
                ExchangeName = "ClientExchnage",
                ExchangeType = ExchangeType.Direct,
                QueueName = "SendResponseQueue",
                RoutingKey = "response"
            });
        }

        public void Run()
        {
            Console.WriteLine("Service run");
        }

        private void MessageReceived(object sender, BasicDeliverEventArgs args)
        {
            var processed = false;
            try
            {
                var value = Encoding.UTF8.GetString(args.Body);
                var toFile = value + " " + DateTime.Now.ToString() + "\n";
                File.WriteAllText(path: $"C:/Навчання/Binary/HomeworksJuly/(3)ServiceCommunication/Output.txt", contents: toFile);
                Console.WriteLine($"Received {value}");
                SendSuccessfulState(value);
                processed = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                processed = false;
            }
            finally
            {
                messageConsumerScope.MessageConsumer.SetAcknowledge(args.DeliveryTag, processed);
            }
        }

        private void SendSuccessfulState(string receivedValue)
        {
            messageProducerScope.MessageProducer.Send(message: $"(client)Received '{receivedValue}'");
        }

        public void Dispose()
        {
            messageConsumerScope.Dispose();
            messageProducerScope.Dispose();
        }
    }



    //class MessageService : IDisposable
    //{
    //    //private readonly IConfiguration configuration;

    //    private IConnection connection;
    //    private IModel channel;
    //    private EventingBasicConsumer consumer;

    //    public MessageService(/*IConfiguration config*/)
    //    {
    //        //configuration = config;
    //    }

    //    private void Configure()
    //    {
    //        var factory = new RabbitMQ.Client.ConnectionFactory
    //        {
    //            //Uri = new Uri(configuration.GetSection("Rabbit").Value)
    //            Uri = new Uri("amqp://guest:guest@localhost:5672")
    //        };

    //        connection = factory.CreateConnection();
    //        channel = connection.CreateModel();
    //        channel.ExchangeDeclare("BetaExchange", ExchangeType.Direct);
    //        channel.QueueDeclare(queue: "BetaQueue",
    //            durable: true,
    //            exclusive: false,
    //            autoDelete: false);
    //        channel.QueueBind("BetaQueue", exchange: "BetaExchange", routingKey: "key");

    //        consumer = new EventingBasicConsumer(channel);
    //        consumer.Received += MessageReceived;

    //        channel.BasicConsume(queue: "BetaQueue",
    //            autoAck: false,
    //            consumer: consumer);
    //    }

    //    private void MessageReceived(object sender, BasicDeliverEventArgs args)
    //    {
    //        var body = args.Body;
    //        var message = Encoding.UTF8.GetString(body);
    //        Console.WriteLine("Message:");
    //        Console.WriteLine(message);
    //        channel.BasicAck(args.DeliveryTag, multiple: false);
    //    }

    //    public void Dispose()
    //    {
    //        connection?.Dispose();
    //        channel?.Dispose();
    //    }
    //}
}
