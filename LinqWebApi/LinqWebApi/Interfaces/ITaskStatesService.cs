﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqWebApi.Models;

namespace LinqWebApi.Interfaces
{
    interface ITaskStatesService
    {
        List<TaskState> GetTaskStates();
        TaskState GetTaskState(int id);
    }
}
