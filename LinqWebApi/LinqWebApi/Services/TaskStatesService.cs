﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using LinqWebApi.Interfaces;
using LinqWebApi.Models;
using Newtonsoft.Json;
using LinqWebApi.Repositories;

namespace LinqWebApi.Services
{
    public class TaskStatesService : ITaskStatesService
    {
        IRepository<TaskState> repository = new TaskStateRepository();
        public List<TaskState> GetTaskStates()
        {
            return repository.GetItemList();
        }

        public TaskState GetTaskState(int id)
        {
            return repository.GetItem(id);
        }

        public void Create(TaskState item)
        {
            repository.Create(item);
        }

        public void Update(int id, TaskState taskState)
        {
            repository.Update(id, taskState);
        }

        public void Delete(int id)
        {
            repository.Delete(id);
        }
    }
}
