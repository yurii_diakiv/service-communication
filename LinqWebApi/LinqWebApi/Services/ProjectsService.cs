﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//using System.Net.Http;
using LinqWebApi.Interfaces;
using LinqWebApi.Models;
using LinqWebApi.Repositories;
//using Newtonsoft.Json;

namespace LinqWebApi.Services
{
    public class ProjectsService : IProjectsService
    {
        IRepository<Project> repository = new ProjectRepository();
        public List<Project> GetProjects()
        {
            return repository.GetItemList();
        }

        public Project GetProject(int id)
        {
            return repository.GetItem(id);
        }

        public void Create(Project item)
        {
            repository.Create(item);
        }

        public void Update(int id, Project project)
        {
            repository.Update(id, project);
        }

        public void Delete(int id)
        {
            repository.Delete(id);
        }
    }
}
