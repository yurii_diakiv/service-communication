﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqWebApi.Models;

namespace LinqWebApi
{
    public class Data
    {
        public List<Project> GetProjects()
        {
            List<Project> projects = new List<Project>();

            Project project1 = new Project
            (
                1,
                "Odit eveniet libero voluptatibus numquam.",
                "Consectetur nisi voluptate vel earum inventore.\nVeniam illum alias aspernatur dolores fuga labore." +
                "\nConsequatur ut neque ut qui ratione consequatur dignissimos corporis." +
                "\nUnde corrupti hic numquam voluptas tempore sed aut modi ut." +
                "\nCorrupti et ipsam voluptas minus iure in voluptatem molestiae.",
                new DateTime(2019, 06, 19),
                new DateTime(2020, 05, 07),
                37,
                2
            );

            Project project2 = new Project
            (
                2,
                "Illo et et molestiae aut.",
                "Ut magnam aliquid quo dolores id.\nEaque quo debitis sint.\nAtque a amet." +
                "\nEst nisi consequatur magni itaque sed blanditiis dicta.",
                new DateTime(2019, 06, 18),
                new DateTime(2019, 07, 26),
                45,
                2
            );

            Project project3 = new Project
            (
                3,
                "Eius fugiat amet molestias eos.",
                "Veniam vitae harum rerum aspernatur iure.\nProvident ipsam nihil possimus impedit at ad nisi.",
                new DateTime(2019, 06, 18),
                new DateTime(2019, 09, 02),
                1,
                1
            );

            Project project4 = new Project
            (
                4,
                "Aliquid qui sed possimus quidem.",
                "Minima laboriosam provident esse quibusdam quibusdam reprehenderit soluta.\nNobis rem velit.",
                new DateTime(2019, 06, 18),
                new DateTime(2020, 04, 19),
                27,
                9
            );

            Project project5 = new Project
            (
                5,
                "Iste est quae beatae et.",
                "Et nihil deserunt.\nVoluptatum velit maxime eos reiciendis laborum.\nConsequatur quibusdam rerum voluptate sit distinctio odit." +
                "\nTotam itaque quis eos quod quas aut facilis et dolore.\nError eos ipsam sapiente exercitationem suscipit sit explicabo." +
                "\nPorro et nemo.",
                new DateTime(2019, 06, 18),
                new DateTime(2020, 06, 01),
                4,
                1
            );

            projects.Add(project1);
            projects.Add(project2);
            projects.Add(project3);
            projects.Add(project4);
            projects.Add(project5);

            return projects;
        }

        public List<Models.Task> GetTasks()
        {
            List<Models.Task> tasks = new List<Models.Task>();

            Models.Task task1 = new Models.Task
            (
                1,
                "Dolorum ullam nesciunt consequuntur quis modi mollitia aliquid doloremque.",
                "Quas ipsa magnam ratione.\nCulpa iusto non quisquam voluptates.\nEst ut non eligendi distinctio asperiores consequatur.",
                new DateTime(2019, 06, 19),
                new DateTime(2019, 11, 30),
                1,
                38,
                31
            );

            Models.Task task2 = new Models.Task
            (
                2,
                "Nemo quo voluptates eius eum.",
                "Doloribus ab omnis expedita aut sit sunt animi.\nEt sunt voluptatem ipsa officiis qui dolores officiis.\nOptio ut magnam est." +
                "\nQuas inventore deserunt aspernatur omnis.",
                new DateTime(2019, 06, 19),
                new DateTime(2019, 02, 05),
                3,
                27,
                15
            );

            Models.Task task3 = new Models.Task
            (
                3,
                "Omnis et et.",
                "Nisi et aut.\nEius natus magni ratione totam.\nNulla tempore incidunt itaque rem.",
                new DateTime(2019, 06, 19),
                new DateTime(2019, 08, 18),
                2,
                73,
                22
            );

            Models.Task task4 = new Models.Task
            (
                4,
                "Veniam voluptatem velit in rem aut ut aut quis.",
                "Earum vel hic rerum sequi.\nNostrum unde molestiae voluptate dolores eius deserunt nobis." +
                "\nTenetur similique officiis officia aut voluptas repellat totam expedita aut.\nQui quasi fugiat est autem.",
                new DateTime(2019, 06, 18),
                new DateTime(2019, 11, 01),
                1,
                39,
                50
            );

            Models.Task task5 = new Models.Task
            (
                5,
                "Voluptatem praesentium eos in.",
                "Aut libero deserunt ut assumenda quo et ducimus sequi sunt.\nDeleniti voluptas omnis enim iure.",
                new DateTime(2019, 06, 19),
                new DateTime(2019, 04, 12),
                3,
                80,
                8
            );

            tasks.Add(task1);
            tasks.Add(task2);
            tasks.Add(task3);
            tasks.Add(task4);
            tasks.Add(task5);

            return tasks;
        }

        public List<TaskState> GetTaskStates()
        {
            List<TaskState> taskStates = new List<TaskState>();

            TaskState taskState1 = new TaskState(0, "Created");
            TaskState taskState2 = new TaskState(1, "Started");
            TaskState taskState3 = new TaskState(2, "Finished");
            TaskState taskState4 = new TaskState(3, "Canceled");

            taskStates.Add(taskState1);
            taskStates.Add(taskState2);
            taskStates.Add(taskState3);
            taskStates.Add(taskState4);

            return taskStates;
        }

        public List<Team> GetTeams()
        {
            List<Team> teams = new List<Team>();

            Team team1 = new Team(1, "labore", new DateTime(2019, 06, 19));
            Team team2 = new Team(2, "reprehenderit", new DateTime(2019, 06, 19));
            Team team3 = new Team(3, "qui", new DateTime(2019, 06, 18));
            Team team4 = new Team(4, "repudiandae", new DateTime(2019, 06, 18));
            Team team5 = new Team(5, "nobis", new DateTime(2019, 06, 19));

            teams.Add(team1);
            teams.Add(team2);
            teams.Add(team3);
            teams.Add(team4);
            teams.Add(team5);

            return teams;
        }

        public List<User> GetUsers()
        {
            List<User> users = new List<User>();

            User user1 = new User
            (
                1,
                "Letha",
                "Strosin",
                "Letha.Strosin58@gmail.com",
                new DateTime(2002, 05, 11),
                new DateTime(2019, 06, 17),
                4
            );

            User user2 = new User
            (
                2,
                "Jeanette",
                "Shields",
                "Jeanette.Shields@yahoo.com",
                new DateTime(2010, 02, 11),
                new DateTime(2019, 05, 17),
                8
            );

            User user3 = new User
            (
                3,
                "Wilbert",
                "Hintz",
                "Wilbert95@hotmail.com",
                new DateTime(2008, 05, 06),
                new DateTime(2019, 05, 11),
                7
            );

            User user4 = new User
            (
                4,
                "Danyka",
                "Bernier",
                "Danyka_Bernier@yahoo.com",
                new DateTime(2006, 07, 08),
                new DateTime(2019, 05, 31),
                7
            );

            User user5 = new User
            (
                5,
                "Kurt",
                "Oberbrunner",
                "Kurt65@gmail.com",
                new DateTime(2008, 01, 16),
                new DateTime(2019, 06, 11),
                5
            );

            users.Add(user1);
            users.Add(user2);
            users.Add(user3);
            users.Add(user4);
            users.Add(user5);

            return users;
        }
    }
}
