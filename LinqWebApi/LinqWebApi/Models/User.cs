﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace LinqWebApi.Models
{
    public class User
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("first_name")]
        public string FirstName { get; set; }
        [JsonProperty("last_name")]
        public string LastName { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("birthday")]
        public DateTime Birthday { get; set; }
        [JsonProperty("registred_at")]
        public DateTime RegistredAt { get; set; }
        [JsonProperty("team_id")]
        public int? TeamId { get; set; }

        public User(int id, string firstName, string lastName, string email, DateTime birthday, DateTime registredAt, int? teamId)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            Birthday = birthday;
            RegistredAt = registredAt;
            TeamId = teamId;
        }



        //public int id { get; set; }
        //public string first_name { get; set; }
        //public string last_name { get; set; }
        //public string email { get; set; }
        //public DateTime birthday { get; set; }
        //public DateTime registred_at { get; set; }
        //public int? team_id { get; set; }

        //public override string ToString()
        //{
        //    return $"id : {id}\nfirst_name : {first_name}\nlast_name : {last_name}\nemail : {email}\nbirthday : {birthday}\n" +
        //        $"registred_at : {registred_at}\nteam_id : {team_id}\n";
        //}
    }
}
