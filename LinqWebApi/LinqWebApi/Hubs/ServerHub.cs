﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace LinqWebApi.Hubs
{
    public sealed class ServerHub : Hub
    {
        public async Task Send(string value)
        {
            await Clients.All.SendAsync(method: "NewValue", value);
        }
    }
}
