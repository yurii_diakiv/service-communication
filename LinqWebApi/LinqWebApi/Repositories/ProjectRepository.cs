﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqWebApi.Models;

namespace LinqWebApi.Repositories
{
    public class ProjectRepository : IRepository<Project>
    {
        public LinqWebApi.Data data = new LinqWebApi.Data();
        List<Project> projects;
        public ProjectRepository()
        {
            projects = data.GetProjects().ToList();
        }
        public List<Project> GetItemList()
        {
            return projects;
        }

        public Project GetItem(int id)
        {
            return projects.Find(p => p.Id == id);
        }

        public void Create(Project p)
        {
            projects.Add(p);
        }

        public void Update(int id, Project p)
        {
            int i = projects.FindIndex(pr => pr.Id == id);
            projects[i] = p;
        }

        public void Delete(int id)
        {
            projects.Remove(projects.Find(p => p.Id == id));
            //Project p = projects.Find(pr => pr.Id == id);
            //if(p != null)
            //{
            //    projects.Remove(p);
            //}
        }
    }
}
