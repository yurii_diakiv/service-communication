﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqWebApi.Models;

namespace LinqWebApi.Repositories
{
    public class TeamRepository : IRepository<Team>
    {
        public LinqWebApi.Data data = new LinqWebApi.Data();
        List<Team> teams = new List<Team>();

        public TeamRepository()
        {
            teams = data.GetTeams().ToList();
        }
        public List<Team> GetItemList()
        {
            return teams;
        }

        public Team GetItem(int id)
        {
            return teams.Find(t => t.Id == id);
        }

        public void Create(Team t)
        {
            teams.Add(t);
        }

        public void Update(int id, Team t)
        {
            int i = teams.FindIndex(te => te.Id == id);
            teams[i] = t;
        }

        public void Delete(int id)
        {
            Team t = teams.Find(te => te.Id == id);
            if (t != null)
            {
                teams.Remove(t);
            }
        }
    }
}
