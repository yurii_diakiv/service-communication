﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LinqWebApi.Repositories
{
    public interface IRepository<T>
    {
        List<T> GetItemList();
        T GetItem(int id);
        void Create(T item);
        void Update(int id, T item);
        void Delete(int id);
    }
}
