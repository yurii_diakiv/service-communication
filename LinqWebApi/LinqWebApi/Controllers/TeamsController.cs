﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LinqWebApi.Services;
using LinqWebApi.Models;

namespace LinqWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private TeamsService teamsService;

        public TeamsController(TeamsService service)
        {
            teamsService = service;
        }

        [HttpGet]
        public List<Team> Get()
        {
            return teamsService.GetTeams();
        }

        [HttpGet("{id}")]
        public Team Get(int id)
        {
            return teamsService.GetTeam(id);
        }

        [HttpPost]
        public void Post([FromBody] Team team)
        {
            teamsService.Create(team);
        }

        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Team team)
        {
            teamsService.Update(id, team);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            teamsService.Delete(id);
        }
    }
}
